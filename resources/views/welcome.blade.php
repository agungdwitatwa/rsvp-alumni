<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Registrasi Acara Beasiswa NTB On The Move">
    <meta name="developer" content="Agung Dwitawa">
    <title>Register | Beasiswa NTB On The Move</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{ url('img/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{ url('img/apple-touch-icon-57x57-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72"
        href="{{ url('img/apple-touch-icon-72x72-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
        href="{{ url('img/apple-touch-icon-114x114-precomposed.png') }}">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap"
        rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/style.css') }}" rel="stylesheet">
    <link href="{{ url('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ url('css/menu.css') }}" rel="stylesheet">
    <link href="{{ url('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/icon_fonts/css/all_icons_min.css') }}" rel="stylesheet">
    <link href="{{ url('css/skins/square/grey.css') }}" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{ url('css/custom.css') }}" rel="stylesheet">

    <script src="{{ url('js/modernizr.js') }}"></script>
    <!-- Modernizr -->

</head>

<body>

    <div id="preloader">
        <div data-loader="circle-side"></div>
    </div><!-- /Preload -->

    <div id="loader_form">
        <div data-loader="circle-side-2"></div>
    </div><!-- /loader_form -->

    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-3">
                    <h4>Beasiswa NTB On The Move</h4>
                </div>
                <div class="col-9">
                    <div id="social">
                        <ul>
                            <li><a href="#0"><i class="icon-facebook"></i></a></li>
                            <li><a href="#0"><i class="icon-instagram"></i></a></li>
                            <li><a href="#0"><i class="icon-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </header>
    <!-- End Header -->

    <main>
        <div id="form_container">
            <div class="row">
                <div class="col-lg-5">
                    <div id="left_form">
                        <figure><img src="img/registration_bg.svg" alt=""></figure>
                        <h2>RSVP</h2>
                        <p>Pastikan data yang Anda input pada form ini merupakan data yang Valid</p>
                        <a href="#0" id="more_info" data-bs-toggle="modal" data-bs-target="#more-info"><i
                                class="pe-7s-info"></i></a>
                    </div>
                </div>
                <div class="col-lg-7">

                    <div id="wizard_container">
                        <div id="top-wizard">
                            <div id="progressbar"></div>
                        </div>
                        <!-- /top-wizard -->
                        {{-- <form action="{{ url('submit-data') }}" name="example-1" id="wrapped" method="post"> --}}
                        <form action="{{ url('submit-data') }}" method="post">
                            @csrf
                            <input id="website" name="website" type="text" value="">
                            <!-- Leave for security protection, read docs for details -->
                            <div id="middle-wizard">
                                <div class="step">
                                    <h3 class="main_question"><strong>1/4</strong>Masukkan data Anda</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="nm_lkp" class="form-control required"
                                                    placeholder="Nama Lengkap Beserta Gelar">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="nohp" class="form-control required"
                                                    placeholder="No.HP">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control required"
                                                    placeholder="Email">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="styled-select">
                                                    <select class="required" name="daerah">
                                                        <option value="" selected>Asal Daerah</option>
                                                        <option value="Kota Mataram">Kota Mataram</option>
                                                        <option value="Lombok Barat">Lombok Barat</option>
                                                        <option value="Lombok Tengah">Lombok Tengah</option>
                                                        <option value="Lombok Timur">Lombok Timur</option>
                                                        <option value="Lombok Utara">Lombok Utara</option>
                                                        <option value="Sumbawa">Sumbawa</option>
                                                        <option value="Sumbawa Barat">Sumbawa Barat</option>
                                                        <option value="Bima">Bima</option>
                                                        <option value="Kota Bima">Kota Bima</option>
                                                        <option value="Dompu">Dompu</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="pekerjaan" class="form-control"
                                                    placeholder="Pekerjaan Saat Ini">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="institusi" class="form-control"
                                                    placeholder="Institusi ">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group radio_input">
                                                <label><input type="radio" value="Laki - Laki" name="jk"
                                                        class="icheck">Laki - Laki</label>
                                                <label><input type="radio" value="Perempuan" name="jk"
                                                        class="icheck">Perempuan</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->
                                </div>
                                <!-- /step-->

                                <div class="step">
                                    <h3 class="main_question"><strong>2/4</strong>Data Tambahan</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" name="domisili" class="form-control required"
                                                    placeholder="Alamat Domisili Sekarang">
                                            </div>
                                        </div>
                                        <!-- /col-sm-12 -->
                                    </div>
                                    <!-- /row -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="styled-select">
                                                    <select class="required" name="negara">
                                                        <option value="">Negara Studi :
                                                        </option>
                                                        <option value="Polandia">Polandia</option>
                                                        <option value="Malaysia">Malaysia</option>
                                                        <option value="China">China</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="jurusan" class="form-control required"
                                                    placeholder="Jurusan">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="styled-select">
                                                    <select class="required" name="thn_brgkt">
                                                        <option value="" selected>Tahun Berangkat Studi :</option>
                                                        <option value="2018">2018</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2020">2020</option>
                                                        <option value="2021">2021</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="styled-select">
                                                    <select class="required" name="thn_lulus">
                                                        <option value="" selected>Tahun Lulus Studi :</option>
                                                        <option value="2018">2018</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2020">2020</option>
                                                        <option value="2021">2021</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->
                                </div>
                                <!-- /step-->
                                <div class="step">
                                    <h3 class="main_question"><strong>3/4</strong>Pilih kegiatan yang akan diikuti</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group radio_input">
                                                <label><input type="checkbox" value="28" name="kegiatan[]"
                                                        class="icheck required">28 November 2021</label>
                                            </div>
                                            <div class="form-group radio_input">
                                                <input type="hidden" name="kegiatan[]" value="29">
                                                <label><input type="checkbox" value="29" name="kegiatan[]"
                                                        class="icheck required" id="wajib" checked disabled>29 November
                                                    2021 (Wajib bagi Alumni)
                                                </label>
                                            </div>
                                            <div class="form-group radio_input">
                                                <label><input type="checkbox" value="30" name="kegiatan[]"
                                                        class="icheck required">30 November 2021</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->
                                </div>
                                <div class="submit step">
                                    <h3 class="main_question"><strong>4/4</strong>Pilih akomodasi</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group radio_input">
                                                <label><input type="radio" value="Menginap di hotel" name="akomodasi"
                                                        class="icheck required">Menginap di hotel</label>
                                            </div>
                                            <div class="form-group radio_input">
                                                <label><input type="radio" value="Menginap di hotel" name="akomodasi"
                                                        class="icheck required">Menginap di tempat lain</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->
                                </div>
                                <!-- /step-->
                            </div>
                            <!-- /middle-wizard -->
                            <div id="bottom-wizard">
                                <button type="button" name="backward" class="backward">Sebelumnya </button>
                                <button type="button" name="forward" class="forward">Selanjutnya</button>
                                <button type="submit" name="process" class="submit">Submit</button>
                            </div>
                            <!-- /bottom-wizard -->
                        </form>
                    </div>
                    <!-- /Wizard container -->
                </div>
            </div><!-- /Row -->
        </div><!-- /Form_container -->
    </main>

    <footer id="home" class="clearfix">
        <p>© 2021 Lembaga Pengembangan Pendidikan NTB</p>
        <ul>
            <li><a href="#0" class="animated_link">Instagram</a></li>
            <li><a href="#0" class="animated_link">Kontak Kami</a></li>
        </ul>
    </footer>
    <!-- end footer-->

    <div class="cd-overlay-nav">
        <span></span>
    </div>

    <!-- Modal info -->
    <!-- /.modal -->

    <!-- SCRIPTS -->
    <!-- Jquery-->
    <script src="{{ url('js/jquery-3.6.0.min.js') }}"></script>
    <!-- Common script -->
    <script src="{{ url('js/common_scripts_min.js') }}"></script>
    <!-- Wizard script -->
    <script src="{{ url('js/registration_wizard_func.js') }}"></script>
    <!-- Menu script -->
    <script src="{{ url('js/velocity.min.js') }}"></script>
    <script src="{{ url('js/main.js') }}"></script>
    <!-- Theme script -->
    <script src="{{ url('js/functions.js') }}"></script>

</body>

</html>
