<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RsvpController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [RsvpController::class, 'landing']);
Route::get('/success', [RsvpController::class, 'success']);
Route::get('/email-already-exists', [RsvpController::class, 'emailExists']);
Route::get('/download-ticket', [RsvpController::class, 'download']);
Route::get('/validate/{id}', [RsvpController::class, 'validation']);

Route::post('/submit-data', [RsvpController::class, 'insertData']);
