<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRsvpDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rsvp_data', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('nm_lkp');
            $table->string('nohp');
            $table->string('email')->unique();
            $table->string('daerah');
            $table->string('jk');
            $table->string('domisili');
            $table->string('negara');
            $table->string('jurusan');
            $table->string('thn_brgkt');
            $table->string('thn_lulus');
            $table->string('kegiatan');
            $table->string('pekerjaan');
            $table->string('institusi');
            $table->string('akomodasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rsvp_data');
    }
}
