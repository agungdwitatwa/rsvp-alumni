<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RsvpData;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class RsvpController extends Controller
{

    public function landing()
    {
        return view('welcome');
    }

    public function insertData(Request $req)
    {
        $emails = RsvpData::all('email');

        foreach ($emails as $email) {

            if ($email->email == $req->input('email')) {
                return redirect('email-already-exists');
            }
        }

        $newData = [
            'id' => strtr(base64_encode('' . date('ymd') . time() .  ''), '+/=', '._-'),
            'nm_lkp' => $req->input('nm_lkp'),
            'nohp' => $req->input('nohp'),
            'email' => $req->input('email'),
            'daerah' => $req->input('daerah'),
            'jk' => $req->input('jk'),
            'domisili' => $req->input('domisili'),
            'negara' => $req->input('negara'),
            'jurusan' => $req->input('jurusan'),
            'thn_brgkt' => $req->input('thn_brgkt'),
            'thn_lulus' => $req->input('thn_lulus'),
            'kegiatan' => implode(", ", $req->input('kegiatan')),
            'pekerjaan' => $req->input('pekerjaan'),
            'institusi' => $req->input('institusi'),
            'akomodasi' => $req->input('akomodasi'),
        ];

        $sessionData = [
            'id' => $newData['id'],
            'nama' => $req->input('nm_lkp'),
            'daerah' => $req->input('daerah'),
            'tgl' => $newData['kegiatan']
        ];

        session($sessionData);

        RsvpData::create($newData);

        return redirect('/success');
    }

    public function success()
    {
        return view('success');
    }

    public function download()
    {
        $img = \Image::make(public_path('img/tanpagaris.png'));

        $img->text(session()->get('nama'), 390, 125, function ($font) {
            $font->file(public_path('font/font.ttf'));
            $font->size(40);
            $font->color('#ffffff');
            $font->align('left');
            $font->valign('top');
        });

        $img->text(session()->get('daerah'), 390, 240, function ($font) {
            $font->file(public_path('font/font.ttf'));
            $font->size(15);
            $font->color('#ffffff');
            $font->align('left');
            $font->valign('top');
        });

        $img->text('' . session()->get('tgl') . " November 2021" . '', 597, 240, function ($font) {
            $font->file(public_path('font/font.ttf'));
            $font->size(15);
            $font->color('#ffffff');
            $font->align('left');
            $font->valign('top');
        });

        $qr = QrCode::format('png')->size(280)->color(14, 43, 92)->generate(url('validate/' . session()->get('id')));

        $img->insert('data:image/jpg;base64,' . base64_encode($qr), 'top-left', 32, 50);

        $name = '' . session()->get('id') . '-' . session()->get('nama') . '.jpg';
        $image = $img->encode('jpg');

        session()->flush();

        $headers = [
            'Content-Type' => 'image/jpg',
            'Content-Disposition' => 'attachment; filename=' . $name,
        ];

        return response()->stream(function () use ($image) {
            echo $image;
        }, 200, $headers);
    }

    public function emailExists()
    {
        return view('emailexist');
    }

    public function validation($id)
    {
        $data = RsvpData::find($id);
        if ($data) {
            echo $data->nm_lkp;
        } else {
            echo 'Tiket tidak valid';
        }
    }
}
